# Ansible Role for Live documentation

This is a Ansible Role to build a documentaion from an ansible deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-livedoc
```
## Features
#### Build a documentation in Markdown

Build a documentation in markdown with some basic key infromations.

```YAML
servicename: template
url: https://template.fhooe.at
bussinesunit: businesunit
costcenter: 0
end_date: none
environment: prod
serviceowner: serviceowner
serviceclass: sla1
legacy: true
managedby:
    - Manual
reference:
    - 1234156
    - 123456
```

Tested:
 - Vagrant Ubuntu 18.04
 - Molecule Ubuntu 18.04
 - Molecule Ubuntu 18.04
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at